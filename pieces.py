class C:
    white = 0
    yellow = 1
    red = 2
    blue = 3
    green = 4
    black = 5

orders = [
        [C.white, C.yellow, C.red, C.black, C.blue, C.green],
        [C.white, C.yellow, C.red, C.black, C.blue, C.green],

        [C.white, C.yellow, C.green, C.red, C.blue, C.black],
        [C.white, C.yellow, C.green, C.red, C.blue, C.black],

        [C.white, C.yellow, C.blue, C.red, C.black, C.green],

        [C.white, C.yellow, C.green, C.black, C.red, C.blue],

        [C.white, C.red, C.black, C.yellow, C.green, C.blue],

        [C.white, C.red, C.black, C.yellow, C.blue, C.green]
        ]
