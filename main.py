#!/usr/bin/env python3
from pieces import orders
import copy

print("Hello world!")

class Orientation:
    def __init__(self, right = 1, forward = 2):
        self.a = [abs(right), abs(forward), 6 - abs(right) - abs(forward)]
        self.o = [1 if right > 0 else -1,\
            1 if forward > 0 else -1,\
            1 if (right * forward > 0) == (abs(right) % 3 + 1 == abs(forward)) else -1]
        self.inv = {}
        for i,e in enumerate([1, 2, 3]):
            self.inv[e] = self.a[i] * self.o[i]
        for i,e in enumerate([-1, -2, -3]):
            self.inv[e] = -self.a[i] * self.o[i]

    def getInv(self, i):
        return self.inv[i]

    def all():
        return [Orientation(k * i, j * ((i%3)+1)) for i in range(1,4) for k in [-1, 1] for j in [-1,1]] \
            + [Orientation(k * ((i%3)+1), j * i) for i in range(1,4) for k in [-1, 1] for j in [-1,1]]

class Block:
    """
    face order : right forward up left backward(fron) bottom

    rotations : 

    """
    iD = 0
    def __init__(self, order):
        self.orderD = {((i + 1) if i < 3 else (2 - i)):o for i,o in enumerate(order)}
        self.ori = Orientation()
        self.pos = (0, 0, 0)
        self.id = Block.iD
        Block.iD += 1

    def setOri(self, Ori):
        self.ori = Ori
    
    def setPos(self, pos):
        self.pos = pos

    def getColor(self, i):
        return self.orderD[self.ori.getInv(i)]

def pTijk(p, n):
    return p // (n * n), (p // n) % n, p % n

def ijkTp(ijk, n):
    return i * n * n + j * n + k

class Cube:
    def __init__(self, n):
        self.n = n
        self.n3 = n*n*n
        self.blocks = [False for i in range(self.n3)]
        for i in range(self.n3):
            print(pTijk(i, n))
        self.colors = {}
        self.blocksList = []
        self.solsCount = 0
        self.sols = []

    def blockCan(self, block, p):
        if self.blocks[p]:
            return False
        i, j, k = pTijk(p, self.n)
        checks = []
        if i == 0:
            checks.append(1)
        elif i == self.n-1:
            checks.append(-1)

        if j == 0:
            checks.append(2)
        elif j == self.n-1:
            checks.append(-2)

        if k == 0:
            checks.append(3)
        elif k == self.n-1:
            checks.append(-3)

        for check in checks:
            if check in self.colors and block.getColor(check) != self.colors[check]:
                return False
        return True

    def setBlock(self, block, p):
        self.blocks[p] = True
        i, j, k = pTijk(p, self.n)
        checks = []
        if i == 0:
            checks.append(1)
        elif i == self.n-1:
            checks.append(-1)

        if j == 0:
            checks.append(2)
        elif j == self.n-1:
            checks.append(-2)

        if k == 0:
            checks.append(3)
        elif k == self.n-1:
            checks.append(-3)

        for check in checks:
            self.colors[check] = block.getColor(check)
        block.setPos((i, j, k))

    def getAll(self, blocks):
        self.blocksList = blocks
        self.getAllI(0)

    def getAllI(self, index):
        pi = 1
        if index == len(self.blocksList):
            self.solsCount += 1
            self.sols.append((copy.deepcopy(self.blocksList), copy.deepcopy(self.colors)))
            return
        if index == pi: print(index, self.colors, len(self.colors))
        blocksCopy = list(self.blocks)
        colorsCopy = dict(self.colors)
        if index == pi: print(colorsCopy)
        for i in range(self.n3):
            for ori in ([Orientation()] if index == 0 else Orientation.all()):
                self.blocks = list(blocksCopy)
                self.colors = dict(colorsCopy)
                self.blocksList[index].setOri(ori)
                if self.blockCan(self.blocksList[index], i):
                    self.setBlock(self.blocksList[index], i)
                    self.getAllI(index + 1)
        if index == pi: print(colorsCopy)
        self.blocks = list(blocksCopy)
        self.colors = dict(colorsCopy)

class C:
    white = 0
    yellow = 1
    red = 2
    blue = 3
    green = 4
    black = 5

bs = [Block(o) for o in orders]
c = Cube(2)
c.getAll(bs)
for sol,cs in c.sols:
    print([b.pos for b in sol])
    print(cs)
#b = Block(order1)
#b.setOri(Orientation(2, 3))
